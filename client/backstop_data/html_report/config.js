report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "..\\bitmaps_reference\\prod_test_Home_0_document_0_phone.png",
        "test": "..\\bitmaps_test\\20161205-224117\\prod_test_Home_0_document_0_phone.png",
        "selector": "document",
        "fileName": "prod_test_Home_0_document_0_phone.png",
        "label": "Home",
        "misMatchThreshold": 0.1,
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.11",
          "analysisTime": 27
        },
        "diffImage": "..\\bitmaps_test\\20161205-224117\\failed_diff_prod_test_Home_0_document_0_phone.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "..\\bitmaps_reference\\prod_test_Home_0_document_1_tablet_v.png",
        "test": "..\\bitmaps_test\\20161205-224117\\prod_test_Home_0_document_1_tablet_v.png",
        "selector": "document",
        "fileName": "prod_test_Home_0_document_1_tablet_v.png",
        "label": "Home",
        "misMatchThreshold": 0.1,
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.03",
          "analysisTime": 75
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "..\\bitmaps_reference\\prod_test_Home_0_document_2_tablet_h.png",
        "test": "..\\bitmaps_test\\20161205-224117\\prod_test_Home_0_document_2_tablet_h.png",
        "selector": "document",
        "fileName": "prod_test_Home_0_document_2_tablet_h.png",
        "label": "Home",
        "misMatchThreshold": 0.1,
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.02",
          "analysisTime": 86
        }
      },
      "status": "pass"
    }
  ]
});