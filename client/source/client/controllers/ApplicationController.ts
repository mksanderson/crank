module Client {
	export class ApplicationController {
		// Inject built in and custom services, they should be
		// passed in as the same order here and in the constructor,
		// so the service and the interface are tethered
		static $inject = [
			'$log',
			'TestService',
		];

		constructor(
			private _logService: ng.ILogService,
			private _testService: ITestService
		) {
			// We can now use our services within the constructor
			// or within class methods
			_logService.log('Application initialised');

			// Use our class method to then go and fetch data using
			// our TestService. You can do this in the constructor as well, directly
			// instead of by way of a class method . This usually depends on whether you only
			// need to do this once at the init or repeatedly at different times,
			// and f
			this.getExamples('/client/api/test/examples.json');
		}
		
		/**
		 * Use TestService to fetch an array of
		 * Examples and log them using $log
		 * 
		 * @param {string} path
		 */
		getExamples(path: string): void{
			this._logService.info('Now we go to the API');
			
			this._testService.get(path).then((response) => {
				this._logService.log(response);
			});
		}
	}

	angular
		.module('Client')
		.controller('ApplicationController', ApplicationController)
}