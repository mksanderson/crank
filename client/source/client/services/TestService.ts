module Client {
	export class TestService implements ITestService {
		static $inject = [
			'$http'
		];

		constructor(private _httpService: ng.IHttpService) {

		}
		
		/**
		 * Check for a keyword in the title of a list
		 * of examples
		 * 
		 * @param {Array<Example>} examples
		 * @param {string} keyword
		 * @returns {boolean}
		 */
		filter(examples: Array<Example>, keyword: string): boolean {
			var result = false;
			
			angular.forEach(examples, (example, exampleID) => {
				if(example.title === keyword){
					result = true;
				}
			});

			return result;
		}

		/**
		 * Go to API path and return data
		 * 
		 * @param {string} path API endpoint
		 * @returns {ng.IHttpPromise<Array<Example>>}
		 */
		get(path: string): ng.IPromise<Array<Example>> {
			var result = this._httpService.get(path).then(function (response) {
				return response.data;
			});

			return result;
		}
	}

	angular
		.module('Client')
		.service('TestService', TestService)
}