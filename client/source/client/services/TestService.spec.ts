module Client {
	describe('TestService', () => {
		var _httpBackend,
			_testService,
			testList = [
				{
					"title": "test"
				},
				{
					"title": "A second test"
				},
			],
			result;

		beforeEach(angular.mock.module('Client'));

		beforeEach(inject(function (_$httpBackend_, _TestService_) {
			_httpBackend = _$httpBackend_;
			_testService = _TestService_;
		}))

		describe('get()', () => {
			it('should return an item with a title', (function (): void {
				var test = function (example) {
					expect(example[0].title).toBe('test')
					expect(example[1].title).toBe('A second test')
				}

				_httpBackend.expectGET('/client/api/test/examples.json').respond(200, testList);

				_testService.get('/client/api/test/examples.json').then(test);

				_httpBackend.flush();
			}))
		})

		describe('filter()', () => {
			it('should return a true value for the title being truthy at least once', (function (): void {
				var result;

				result = _testService.filter(testList, 'test');

				expect(result).toBe(true);
			}))
		})
	})
}