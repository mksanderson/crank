// Depdencies
import 'angular';

// Application startup
import './startup';

// Controllers
import './Controllers/ApplicationController';

// Components

// Directives

// Services
import './Services/TestService';