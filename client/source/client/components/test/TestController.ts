module Client {
	export class TestController {
		static $inject = [

		];

		public state: boolean;

		constructor() {
			this.state = false;
		}
		
		/**
		 * Check if state is truthy
		 * 
		 * @returns {boolean}
		 * 
		 * @memberOf TestController
		 */
		isToggled(): boolean{
			if(this.state){
				return true;
			}
			else{
				return false;
			}
		}

		/**
		 * Toggle state variable
		 */
		toggle(): void {
			this.state = !this.state;
		}
	}
}