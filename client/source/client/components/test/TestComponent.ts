module Client {
	export class TestComponent implements ng.IComponentOptions {
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;
		public transclude: any;

		constructor() {
			this.controller = TestController;
			this.controllerAs = 'Test';
			this.templateUrl = '/components/test/test.html';
		}
	}

	angular
		.module('Client')
		.component('test', new TestComponent);
}