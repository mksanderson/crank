module Client {
	export class ExampleDirective implements ng.IDirective {
		public controller: any;
		public controllerAs: any;
		public templateUrl: string;

		constructor() {
			this.controller = ExampleController;
			this.controllerAs = 'Example';
			this.templateUrl = '/client/directives/example/example.html'
		}

		static instance(): ng.IDirective {
			return new ExampleDirective();
		}

		public link(scope: ng.IScope, element: ng.IAugmentedJQuery): void {

		}
	}

	angular
		.module('Client')
		.directive('dropdown', ExampleDirective.instance)
}