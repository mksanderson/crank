module Client{
	// Get Angular application started
	angular.element(document).ready(function(){
		angular.bootstrap(document, ['Client']);
	});

	// Initialise imported modules
	angular.module('Client', []);
}