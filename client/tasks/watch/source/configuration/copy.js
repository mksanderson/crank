module.exports = function (gulp, plugins) {
	plugins.watch([
		'source/*.{txt, xml}',
		'source/browser/*'
	], function (events, done) {
		gulp.start('copy:source:configuration');
	});
}