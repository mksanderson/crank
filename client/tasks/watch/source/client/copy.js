module.exports = function (gulp, plugins) {
	return function () {
		plugins.watch('source/client/**/*.{js,json}', function(events, done) {
			gulp.start('copy:source:client');
		})
	};
}