module.exports = function (gulp, plugins) {
	return function () {
		plugins.watch('source/client/**/*.pug', function(events, done) {
			gulp.start('pug:source:client');
		});
	};
}