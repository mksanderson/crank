module.exports = function (gulp, plugins) {
	return function () {
		plugins.watch('source/client/**/*.ts', function(events, done) {
			gulp.start('typescript:source:client');
		});
	};
}