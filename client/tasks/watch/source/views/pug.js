module.exports = function (gulp, plugins) {
	return function () {
		plugins.watch('source/views/**/*.pug', function (events, done) {
			gulp.start('pug:source:views');
		});
	};
}