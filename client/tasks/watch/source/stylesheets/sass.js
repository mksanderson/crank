module.exports = function (gulp, plugins) {
	return function () {
		plugins.watch('source/**/*.scss', function (events, done) {
			gulp.start('sass:source:stylesheets');
		});
	};
}