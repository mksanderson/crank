module.exports = function (gulp, plugins) {
	return function () {
		plugins.watch('source/images/**/*', function (events, done) {
			gulp.start('imagemin:source:images');
		});
	};
}