module.exports = function (gulp, plugins) {
	return function () {
		plugins.watch('source/images/**/*.svg', function (events, done) {
			gulp.start('svgSymbols:source:graphics');
		});
	};
}