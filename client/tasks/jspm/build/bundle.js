module.exports = function (gulp, plugins) {
	return function () {
		return gulp.src('wwwroot/client/bootstrap.js', { read: false })
			.pipe(plugins.plumber())
			.pipe(plugins.jspm({
				fileName: 'client',
				selfExecutingBundle: true
			}))
			.pipe(gulp.dest('../server/'))
	}
}