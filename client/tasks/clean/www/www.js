module.exports = function (gulp, plugins) {
	return function () {
		return gulp.src('wwwroot/', { read: false })
			.pipe(plugins.clean())
	}
}