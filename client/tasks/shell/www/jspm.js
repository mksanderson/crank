module.exports = function (gulp, plugins) {
	return function () {
		return gulp.src('*.js', { read: false })
			.pipe(plugins.shell([
				'jspm i -y'
			]));
	}
}