module.exports = function (gulp, plugins) {
	return function () {
		return gulp.src('source/images/**/*')
			.pipe(plugins.changed('wwwroot/images/'))
			.pipe(plugins.plumber())
			.pipe(gulp.dest('wwwroot/images/'))
			.pipe(plugins.browserSync.stream());
	};
}