module.exports = function (gulp, plugins) {
	return function () {
		return gulp.src([
			'source/client/**/*.ts',
		])
			.pipe(plugins.plumber())
			.pipe(plugins.sourcemaps.init())
			.pipe(plugins.typescript())
			.pipe(plugins.sourcemaps.write())
			.pipe(gulp.dest('wwwroot/client'))
			.pipe(plugins.browserSync.stream());
	}
}