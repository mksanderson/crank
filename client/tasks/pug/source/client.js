module.exports = function (gulp, plugins) {
	return function () {
		var store = {};

		return gulp.src('source/client/**/*.pug')
			.pipe(plugins.plumber())
			.pipe(plugins.changed('source/client', { extension: '.pug' }))
			.pipe(plugins.pug({
				basedir: 'source/client',
				locals: store,
				pretty: true
			}))
			.pipe(gulp.dest('wwwroot/client/'))
			.on('end', function () {
				plugins.browserSync.reload();
			})
	}
}