module.exports = function (gulp, plugins) {
	return function () {
		return gulp.src('source/client/**/*.{js,json}')
			.pipe(plugins.plumber())
			.pipe(gulp.dest('wwwroot/client/'))
			.pipe(plugins.browserSync.stream());
	}
}