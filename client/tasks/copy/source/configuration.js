module.exports = function(gulp, plugins) {
	return function() {
		return gulp.src(['source/browser/**/*','source/*.{xml, txt}'])
			.pipe(plugins.plumber())
			.pipe(gulp.dest('wwwroot/'))
			.pipe(plugins.browserSync.stream());
	}
}