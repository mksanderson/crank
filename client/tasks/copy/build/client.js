module.exports = function(gulp, plugins) {
	return function() {
		return gulp.src('wwwroot/client/**/*')
			.pipe(plugins.plumber())
			.pipe(gulp.dest('../server/client/'))
	}
}