var gulp = require('gulp'),
	plugins = require('gulp-load-plugins')({
		pattern: '*'
	});

// BrowserSync
// Path: tasks/browser-sync
gulp.task('browserSync:www', ['browserSync:www:server']);
gulp.task('browserSync:www:server', require('./tasks/browser-sync/www/server')(gulp, plugins));

// Clean
// Path: tasks/clean
gulp.task('clean:www', ['clean:www:www']);
gulp.task('clean:www:www', require('./tasks/clean/www/www')(gulp, plugins));

// Copy
// Path: tasks/copy
gulp.task('copy:source', [
	'copy:source:client',
	'copy:source:configuration']);
gulp.task('copy:source:client', require('./tasks/copy/source/client')(gulp, plugins));
gulp.task('copy:source:configuration', require('./tasks/copy/source/configuration')(gulp, plugins));

gulp.task('copy:build', [
	'copy:build:client']);
gulp.task('copy:build:client', require('./tasks/copy/build/client')(gulp, plugins));

// JSPM
// Path: tasks/jspm
gulp.task('jspm:build', ['jspm:build:bundle']);
gulp.task('jspm:build:bundle', require('./tasks/jspm/build/bundle')(gulp, plugins));

// Pug
// Path: tasks/copy
gulp.task('pug:source', [
	'pug:source:client',
	'pug:source:views']);
gulp.task('pug:source:client', require('./tasks/pug/source/client')(gulp, plugins));
gulp.task('pug:source:views', require('./tasks/pug/source/views')(gulp, plugins));

// Imagemin
// Path: tasks/image-min
gulp.task('imagemin:source', ['imagemin:source:images']);
gulp.task('imagemin:source:images', require('./tasks/image-min/source/images')(gulp, plugins));

// Sass
// Path: tasks/sass
gulp.task('sass:source', ['sass:source:stylesheets']);
gulp.task('sass:source:stylesheets', require('./tasks/sass/source/stylesheets')(gulp, plugins));

gulp.task('sass:build', ['sass:build:stylesheets']);
gulp.task('sass:build:stylesheets', require('./tasks/sass/build/stylesheets')(gulp, plugins));

// Shell
// Path: tasks/shell
gulp.task('shell:www', ['shell:www:backstop', 'shell:www:karma', 'shell:www:jspm']);
gulp.task('shell:www:backstop:reference', require('./tasks/shell/www/backstop/reference')(gulp, plugins));
gulp.task('shell:www:backstop:test', require('./tasks/shell/www/backstop/test')(gulp, plugins));
gulp.task('shell:www:karma', require('./tasks/shell/www/karma')(gulp, plugins));
gulp.task('shell:www:jspm', require('./tasks/shell/www/jspm')(gulp, plugins));

// SVG Symbols
// Path: tasks/svg-symbols
gulp.task('svgSymbols:source', ['svgSymbols:source:graphics']);
gulp.task('svgSymbols:source:graphics', require('./tasks/svg-symbols/source/graphics')(gulp, plugins));

// TypeScript
// Path: tasks/typescript
// Runs over the client application and converts and exports to wwwroot
gulp.task('typescript:source', [
	'typescript:source:client'
]);
gulp.task('typescript:source:client', require('./tasks/typescript/source/client')(gulp, plugins));

// Watch
// Path: tasks/watch
gulp.task('watch:source', [
	'watch:source:client:copy',
	'watch:source:client:pug',
	'watch:source:client:typescript',
	'watch:source:images:image-min',
	'watch:source:images:svg-symbols',
	'watch:source:stylesheets:sass',
	'watch:source:views:pug']);
gulp.task('watch:source:client:copy', require('./tasks/watch/source/client/copy')(gulp, plugins));
gulp.task('watch:source:client:pug', require('./tasks/watch/source/client/pug')(gulp, plugins));
gulp.task('watch:source:client:typescript', require('./tasks/watch/source/client/typescript')(gulp, plugins));
gulp.task('watch:source:images:image-min', require('./tasks/watch/source/images/image-min')(gulp, plugins));
gulp.task('watch:source:images:svg-symbols', require('./tasks/watch/source/images/svg-symbols')(gulp, plugins));
gulp.task('watch:source:stylesheets:sass', require('./tasks/watch/source/stylesheets/sass')(gulp, plugins));
gulp.task('watch:source:views:pug', require('./tasks/watch/source/views/pug')(gulp, plugins));

// Command: gulp build
// Send client side assets to relevant
// server side locations (you may need to update these,
// default is ../server/)
gulp.task('build', ['clean:www', 'static'], function () {
	gulp.start('copy:build');
	gulp.start('jspm:build');
	gulp.start('sass:build');
});

// Command: gulp
// Run this the first time the project needs to be set up
// or when it needs to be reinitialised
gulp.task('default', ['tidy'], function () {
	gulp.start('shell:www:jspm');
	gulp.start('shell:www:backstop:reference');
});

// Command: serve
// Build out the project and set up a server
gulp.task('serve', ['static'], function () {
	gulp.start('server')
})

// Command: gulp server
// Set up all servers
gulp.task('server', ['watch:source'], function () {
	gulp.start('browserSync:www');
	gulp.start('shell:www:karma');
});

// Command: gulp static
// Build out all assets
gulp.task('static', ['typescript:source', 'sass:source', 'copy:source', 'imagemin:source', 'pug:source'], function () {
	gulp.start('svgSymbols:source');
});

// Command: gulp test
// Start up a server and test
gulp.task('test', ['static'], function () {
	gulp.start('shell:www:karma');
	gulp.start('shell:www:backstop:test');
})

// Command: gulp tidy
// Clean wwwroot and rebuild it
gulp.task('tidy', ['clean:www'], function () {
	gulp.start('static');
});